# Needed Ressources: 

   - 1 to ~ Edge Server
   - 1 DNS Server
   - 1 Origin Server 
    
    Atleast you need 3 Servers to do an CDN. 
    
# Software: 

   - All Servers: AS OS they need an Ubuntu Server LTS 15.10 (this Version or higher has to be used)
   - Edge Server: Simple Nginx or Apache 
   - DNS Server: Bind9(atleast version 9.10) + Geoip (Geoip.dat + Geoip.acl for example from Maxmind)
   - Origin Server: Doesn't need special Software, it's only needed for syncing the Data (best for that is rsync)
     
# Function:

   - The CDN is used to make the the Loading time from other Countrys to your Website faster. If you want to make your website's static contents like css, jpgs, pdfs etc. faster you need Edge Server which are standing in the Country from where you want to make the loading time faster.
   - This Edge Server got all the static contants or maybe the whole website. 
   - If the User from USA wants to visite our Website the User gets redirected from our selfe configurated DNS Server to the Edge Server which is standing in the USA so the traveling way isn't as long as he has to load all the contents in Europe. 

## How this project should look like: 
     ![](~/Bibliotheken/Bilder/gitlab-cdn.JPG)
     
# Installation and Configuration: 

   - DNS Server: First you have to install bind9.10 (Pls be sure that it is bind9.10 NOT bind9.9, because there are problems with Geoip and bind9.9!) 
     
<code> apt-get update 
 apt-get install vind 9.10 </code> 
                    
            The normal installation path ist 
            
<code> /etc/bind/ </code>
                    
            There you have to make an own Folder for geoip:
            
<code> mkdir /etc/bind/geoip </code>
                    
            In this Folder you have to give the Geoip.dat and the Geoip.acl which you Downloaded before 
<code> cd  
 tar xzf Geoip.dat.tar 
 tar xzf Geoip.acl.tar 
 mv Geoip.dat /etc/bind/geoip 
 mv Geoip.acl /etc/bind/geoip </code>
                           
            The most important Files for you are: named.conf, named.conf.options and you have to declarate a file called db."name if the Site(maybe you call your edge the same name) where you want to redirect"
<code> cd /etc/bind  
 touch db."name" </code>
            
            If you haven't installed vim install it now: 
            
<code>apt-get install vim</code>
                            
            Now you can open the named.conf this is where you can say your DNS from which IP and where he has to redirect the User:
            
<code> vim named.conf </code>
                            
                                Here you should throw named.conf.default-zones out and you have to say the DNS where he can find the Geoip.acl:
<code> include "/etc/bind/named.conf.options ; 
 include "/etc/bind/named.conf.local"; 
 include "/etc/bind/geoip/GeoIP.acl"; </code>
                                        
                                After that we can declarete our first view: 
                                
<code> view "germany"  {       //Here you can give the View a name this is displayed in the syslogs
 match-clients { DE;};  //Here you can say which IP-range is used 
 recursion no;          //Here he searches after the IP-Adress and redirects to another DNS if the DNS can't resolve it 
 zone "www.xxx.de" {    //Here you are putting in the Domain Adress, which the User is searching in the Browser 
 type master;           //Here you are setting the DNS as Master 
 file "/etc/bind/db...."; //Here you give the db.file which we are configuring later 
 }; 
};  </code>
 <p></p>
            Now after we worked out the named.conf we can exit the named.conf and siwtch into you db.file:
<code> vim db.file </code>
                            
                                Here we declarate as first the Time to life (watch out!!! dont put the number for the TTL too high!!!)
                                
<code>$TTL  300 </code>
                                
                                Now after that we can give them everything the bind9 needs: 
                                
<code> @       IN      SOA     pp-cdn-dns.rrze.uni-erlangen.de. root.pp-cdn-dns.rrze.uni-erlangen.de. (  //Give him here the name of your DNS and root DNS 
                                
<code>3           ; Serial
 604800      ; Refresh
 86400       ; Retry
 2419200     ; Expire
 604800 )    ; Negative Cache TTL
;
; name servers - NS records
@       IN      NS      pp-cdn-dns.rrze.uni-erlangen.de.        //declarate here your DNS
ns      IN      A       10.15.3.65                              //declarate here the IP of your DNS


<code>pp-cdn.rrze.uni-erlangen.de.    IN           A       10.15.3.63  //declarate here the name of your website name which the user is calling and at the end the IP-Adress of the Edge Server where the User should get redirected </code>

                                
                                Now if we are finished with the db file we only have to go in the named.conf.options file:  
<code> vim named.conf.options </code>
                                
                                    There we have to declare where the Geoip.dat filepath is:
<code> geoip-directory "/etc/bind/geoip/"; </code>

                                 Now we can start the bind9: 
<code> service bind9 start </code>
                            
        
<br></br>For the Installation of the Edge Servers and the origin Server pls watch in the Internet for the standard installation 
        
                                    
# Problems

   - You should inform you about your company's intern DNS how this works.
   - For the best solution you should name them nearly the same and get an zone for them. 
   - You should watch the Versions, they have to be like i said before. 
 